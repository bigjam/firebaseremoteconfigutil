﻿using UnityEngine;
using System.Collections;
using BicDB.Container;
using System.Linq;
using BicDB.Variable;

namespace BicDB.Storage
{
	public class FRTableContainer : TableContainer<FRRecordContainer>{
		public FRTableContainer() : base("remoteConfigTable"){
			PrimaryKey = "key";
			SetStorage(FRStorage.GetInstance());
		}

		public void AddDefault(string _key, string _value){
			Add(new FRRecordContainer(this, _key, _value));
		}

		public IVariable GetValue(string _key){
			var _findRow = this.FirstOrDefault(_row=>_row.GetValue<IVariable>("key").AsString==_key);

			if (_findRow == null) {
				return null;
			}

			return _findRow.Value;
		}
	} 
}