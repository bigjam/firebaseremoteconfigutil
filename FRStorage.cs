﻿using System.Collections;
using System.Collections.Generic;
using Firebase.RemoteConfig;
using System.Threading.Tasks;
using System;
using BicDB.Variable;
using System.Linq;
using BicDB.Container;
using System.Diagnostics;
using UnityEngine;

namespace BicDB.Storage
{
	public class FRStorage : ITableStorage 
	{
		#region static
		static private FRStorage instance = null;
		static public FRStorage GetInstance(){
			if (instance == null) {
				instance = new FRStorage();
			}

			return instance;
		}
		#endregion

		public enum ResultCode
		{
			Success = 0,
			FailedFetch = 1
		}

		#region Storage

		public void Save<T>(BicDB.ITableContainer<T> _table, Action<Result> _callback, object _parameter) where T : BicDB.IRecordContainer, new ()
		{
			throw new NotImplementedException();
		}

		public void Load<T>(BicDB.ITableContainer<T> _table, Action<Result> _callback, object _parameter) where T : BicDB.IRecordContainer, new ()
		{
			_table.Clear();
			getRemoteConfig(_table, _callback);
		}

		public void Pull<T>(BicDB.ITableContainer<T> _table, Action<Result> _callback, object _parameter) where T : BicDB.IRecordContainer, new ()
		{
			getRemoteConfig(_table, _callback);
		}

		private void getRemoteConfig<T>(BicDB.ITableContainer<T> _table, Action<Result> _callback) where T : BicDB.IRecordContainer, new ()
		{
			FirebaseRemoteConfig.FetchAsync(System.TimeSpan.Zero).ContinueWith(_task=>{
				if(_task.IsFaulted && !_task.IsCompleted){
					if(_callback != null){
						_callback(new Result((int)ResultCode.FailedFetch));
					}
				}else{
					FirebaseRemoteConfig.ActivateFetched();

					var _keys = FirebaseRemoteConfig.Keys;

					foreach (var _key in _keys) {
						var _find = _table.FirstOrDefault(_row=>_row["key"].AsVariable.AsString == _key);
						if(_find != null){
							_find["value"].AsVariable.AsString = FirebaseRemoteConfig.GetValue(_key).StringValue;
						}else{
							var _record = new T();
							(_record as FRRecordContainer).Key.AsString = _key;
							(_record as FRRecordContainer).Value.AsString = FirebaseRemoteConfig.GetValue(_key).StringValue;
							_table.Add(_record);
						}
					}

					if(_callback != null){
						_callback(new Result((int)ResultCode.Success));
					}
				}
			});
		}
		#endregion
	}
}