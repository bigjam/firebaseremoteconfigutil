﻿using System.Collections;
using BicDB.Container;
using BicDB.Variable;

namespace BicDB.Storage
{
	public class FRRecordContainer : RecordContainer{
		public StringVariable Key = new StringVariable();
		public StringVariable Value = new StringVariable();

		public FRRecordContainer(){
			Add("key", Key);
			Add("value", Value);
		}

		public FRRecordContainer(IRecordContainerParent _parent, string _key, string _value) : this(){
			Parent = _parent;
			Key.AsString = _key;
			Value.AsString = _value;
		}
	}
}